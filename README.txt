
Description
-----------
This simple module was created to handle the formatting of entity tokens.

When using certain field entities the entity token module will provide formatting and data sanitisation for display. However, there are times when may need to use the default value of the original field.

How to use this module
After install on the field edit page use the select box to choose the raw() or value() output of the entity, instead of the default format from the entity token module. In many cases the output is likely to be the same depending on the format. It uses the Entity Meta Wrapper to obtain these values, discussed further here: https://www.drupal.org/documentation/entity-metadata-wrappers

In my use-case I needed the key value from a field select list, in-order for the Pathauto module to generate a URL alias. The Entity Token module always displays the label of a field if one is available. In this case I did not want to use the label value in the URL.

It was made in response to these discussions:
https://www.drupal.org/node/1781498
https://www.drupal.org/node/2082407

Requirements
------------
Drupal 7.x
See https://www.drupal.org/sandbox/roboco/2709617 for additional requirements.

Installation
------------
1. Copy the module to Drupal module directory: sites/all/modules

2. Login as an administrator. Enable the module in the "Administer" -> "Modules"

Use
---

1. Create a new content type or choose an existing.

2. Go to the content types manage fields page, and click edit on a field you wish to change the display token on.

3. On the field edit page choose the toke format value() or raw().

4. When ever you use this token on the site it will now use the alternative format patterns.